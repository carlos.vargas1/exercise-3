package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private WebDriver driver;

    private By dropdownBy = By.id("find_desc");
    private By restaurantBy = By.xpath("//*[text()='Restaurants']");
    private String url = "https://www.yelp.com/";

    public MainPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getDropDown(){
        return driver.findElement(dropdownBy);
    }

    public WebElement getRestaurant(){
        return driver.findElement(restaurantBy);
    }

    public String getUrl(){
        return url;
    }
}