package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class ResultPage {
    private WebDriver driver;

    private By address1By = By.xpath("//address/p[1]");
    private By address2By = By.xpath("//address/p[2]");
    private By webLinkBy = By.xpath("((//section[contains(., 'Get Directions')]/div/div)[1]//p)[2]/a");
    private By phoneNumberBy = By.xpath("((//section[contains(., 'Get Directions')]/div/div)[2]//p)[2]");
    private By comment1By = By.xpath("(//span[contains(@lang,'en')])[1]");
    private By comment2By = By.xpath("(//span[contains(@lang,'en')])[2]");
    private By comment3By = By.xpath("(//span[contains(@lang,'en')])[3]");

    public ResultPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getAddress1(){
        return driver.findElement(address1By);
    }

    public WebElement getAddress2(){
        return driver.findElement(address2By);
    }

    public WebElement getWebLink() {
        return driver.findElement(webLinkBy);
    }

    public WebElement getPhoneNumber() {
        return driver.findElement(phoneNumberBy);
    }

    public WebElement getComment1(){
        return driver.findElement(comment1By);
    }

    public WebElement getComment2(){
        return driver.findElement(comment2By);
    }

    public WebElement getComment3(){
        return driver.findElement(comment3By);
    }
}
