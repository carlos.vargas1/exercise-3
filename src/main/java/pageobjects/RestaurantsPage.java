package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class RestaurantsPage {
    private WebDriver driver;

    private By searchBoxBy = By.name("find_desc");
    private By restaurantBy = By.xpath("//*[text()='Restaurants']");
    private String resultsByXpathPart1 = "//ul[contains(.,'All Results')]/li[descendant::span[contains(.,'.')]]";
    private String resultsByXpathPart2 = "//a[normalize-space(text())])[1]";
    private String resultsByXpathStars = "//div[contains(@aria-label,'star rating')]";
    private By filter1By = By.xpath("//*[contains(@value,'RestaurantsGoodForGroups')]");
    private By filter2By = By.xpath("//*[contains(@value,'GoodForKids')]");
//    private By firstResultBy = By.xpath("(//ul[contains(.,'All Results')]/li[descendant::span[contains(.,'.')]])[1]//a[contains(., 'more')]");
//    $x("(//ul[contains(.,'All Results')]/li[descendant::span[contains(.,'.')]])[1]//a[contains(., 'more')]");
    private By firstResultBy = By.xpath("(" + resultsByXpathPart1 + "[1]" + resultsByXpathPart2);
    private List <String> resultList;
    private List <String> starList;

    public RestaurantsPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchBox(){
        return  driver.findElement(searchBoxBy);
    }

    public WebElement getRestaurant(){
        return driver.findElement(restaurantBy);
    }

    public WebElement getFilter1(){
        return driver.findElement(filter1By);
    }

    public WebElement getFilter2(){
        return driver.findElement(filter2By);
    }

    public void calculateResults(){
        List<WebElement> list = driver.findElements(By.xpath(resultsByXpathPart1));
        int size = list.size();
        resultList = new ArrayList<>();
        starList = new ArrayList<>();

        for(int i=1; i<=size; i++){
            String textName = driver.findElement(By.xpath("(" + resultsByXpathPart1 + "[" + i + "]" + resultsByXpathPart2) ).getText();
            String textStar = driver.findElement(By.xpath("(" + resultsByXpathPart1 + ")[" + i + "]" + resultsByXpathStars) ).getAttribute("aria-label");
            resultList.add(i + ". " + textName);
            starList.add(textStar);
        }
    }

    public List<String> getResultList() {
        return resultList;
    }

    public List<String> getStarList() {
        return starList;
    }

    public WebElement getFirstResult(){
        return driver.findElement(firstResultBy);
    }
}
