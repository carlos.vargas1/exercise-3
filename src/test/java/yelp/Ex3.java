package yelp;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pageobjects.MainPage;
import pageobjects.RestaurantsPage;
import pageobjects.ResultPage;
import resources.Base;

import java.io.IOException;
import java.util.List;

public class Ex3 extends Base {
    MainPage mainPage;
    RestaurantsPage restaurantsPage;
    ResultPage resultPage;
    WebDriver driver;

    @BeforeTest
    public void initialize() throws IOException {
        driver = initializeDriver();
        mainPage = new MainPage(driver);
        restaurantsPage = new RestaurantsPage(driver);
        resultPage = new ResultPage(driver);
    }

    @Test
    public void test(ITestContext context) throws InterruptedException {

        System.out.println(mainPage.getUrl());
//        Go to yelp.com
        driver.get(mainPage.getUrl());
//        Select “Restaurants” in the dropdown box in Find.
        mainPage.getDropDown().click();
        mainPage.getRestaurant().click();
//        Append Pizza to Restaurants to make the search text as “Restaurants Pizza”
        Thread.sleep(3000);
        restaurantsPage.getSearchBox().click();
        restaurantsPage.getRestaurant().click();
        restaurantsPage.getSearchBox().sendKeys(" Pizza");
//        Report total no. of Search results with no. of results in the current page (on console and test report).
        Thread.sleep(3000);
        restaurantsPage.calculateResults();
        printResults(restaurantsPage.getResultList());
        context.setAttribute("resultList1", restaurantsPage.getResultList());
//        Parameterize any 2 of the filtering parameters (Neighborhoods, Distance, Price, Features, etc.).
//        Apply the filter.
        Actions actions = new Actions(driver);
        actions.moveToElement(restaurantsPage.getFilter1()).click().perform();
        actions.moveToElement(restaurantsPage.getFilter2()).click().perform();
        Thread.sleep(5000);
        restaurantsPage.calculateResults();
//        Report total no. of Search results with no. of results in the current page (on console and test report)
        printResults(restaurantsPage.getResultList());
//        Report the star rating of each of the results in the first result page (on console and test report)
        printStarResults(restaurantsPage.getResultList(), restaurantsPage.getStarList());
//        Go into the first result from the search results
//        actions.moveToElement(restaurantsPage.getFirstResult()).click().perform();
        restaurantsPage.getFirstResult().click();
//        Log all critical information of the selected restaurant:
        Thread.sleep(5000);
        System.out.println("");
        System.out.println("Address1:\t" + resultPage.getAddress1().getText());
        System.out.println("Address2:\t" + resultPage.getAddress2().getText());
        System.out.println("Webpage:\t" + resultPage.getWebLink().getText());
        System.out.println("Phone Number:\t" + resultPage.getPhoneNumber().getText());
        System.out.println("Comment1:\n" + resultPage.getComment1().getText());
        System.out.println("========================================");
        System.out.println("Comment2:\n" + resultPage.getComment2().getText());
        System.out.println("========================================");
        System.out.println("Comment3:\n" + resultPage.getComment3().getText());
        System.out.println("");
        context.setAttribute("resultList2", restaurantsPage.getResultList());
        context.setAttribute("starList", restaurantsPage.getStarList());
        context.setAttribute("address1", resultPage.getAddress1().getText());
        context.setAttribute("address2", resultPage.getAddress2().getText());
        context.setAttribute("webpage", resultPage.getWebLink().getText());
        context.setAttribute("phone", resultPage.getPhoneNumber().getText());
        context.setAttribute("comment1", resultPage.getComment1().getText());
        context.setAttribute("comment2", resultPage.getComment2().getText());
        context.setAttribute("comment3", resultPage.getComment3().getText());
    }

    @AfterTest
    public void closeDriver(){
        driver.close();
    }

    private void printResults(List<String> list){
        for (String element:list) {
            System.out.println(element);
        }
        System.out.println("");
    }

    private void printStarResults(List<String> list1, List<String> list2){
        for(int i=0; i<list1.size(); i++){
            System.out.println(list1.get(i) + "\t" + list2.get(i));
        }
    }
}
