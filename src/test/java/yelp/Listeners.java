package yelp;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import resources.Base;
import resources.ExtendReporterNG;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class Listeners extends Base implements ITestListener {
    ExtentTest test;
    ExtentReports extent = ExtendReporterNG.getReportObject();
    ThreadLocal<ExtentTest> extentTest =new ThreadLocal<ExtentTest>();

    @Override
    public void onTestStart(ITestResult result) {
        test = extent.createTest(result.getMethod().getMethodName());
        extentTest.set(test);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        extentTest.get().log(Status.PASS, "Test Passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        //Screenshot
        extentTest.get().fail(result.getThrowable());
        WebDriver driver = null;
        String testMethodName = result.getMethod().getMethodName();

        try {
            driver = (WebDriver) result.getTestClass().getRealClass().getDeclaredField("driver").get(result.getInstance());
        } catch (Exception e) {

        }
        try {
            extentTest.get().addScreenCaptureFromPath(getScreenShotPath(testMethodName, driver), result.getMethod().getMethodName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext result) {

    }

    @Override
    public void onFinish(ITestContext result) {
//        String url = (String) result.getAttribute("url");
//        List listRestult1;
//        List listResult2;
//        List starResult;
//
//        listRestult1 = (List) result.getAttribute("resultList1");
//        extentTest.get().log(Status.INFO, "first result");
//        for (Object element : listRestult1) {
//            extentTest.get().log(Status.INFO, element.toString());
//        }
//
//        listResult2 = (List) result.getAttribute("resultList2");
//        extentTest.get().log(Status.INFO, "second result");
//        for (Object element : listResult2) {
//            extentTest.get().log(Status.INFO, element.toString());
//        }
//
//        starResult = (List) result.getAttribute("starList");
//        extentTest.get().log(Status.INFO, "result with stars");
//        for (int i = 0; i < listResult2.size(); i++) {
//            extentTest.get().log(Status.INFO, listResult2.get(i).toString() + "\t" + starResult.get(i).toString());
//        }
//
//        extentTest.get().log(Status.INFO, "address1:\t" + result.getAttribute("address1").toString());
//        extentTest.get().log(Status.INFO, "address2:\t" + result.getAttribute("address2").toString());
//        extentTest.get().log(Status.INFO, "webpage:\t" + result.getAttribute("webpage").toString());
//        extentTest.get().log(Status.INFO, "phone:\t" + result.getAttribute("phone").toString());
//        extentTest.get().log(Status.INFO, "comment1:\t" + result.getAttribute("comment1").toString());
//        extentTest.get().log(Status.INFO, "comment2:\t" + result.getAttribute("comment2").toString());
//        extentTest.get().log(Status.INFO, "comment3:\t" + result.getAttribute("comment3").toString());

        extent.flush();
    }
}
